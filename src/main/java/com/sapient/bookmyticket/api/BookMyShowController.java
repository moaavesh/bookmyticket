package com.sapient.bookmyticket.api;

import com.sapient.bookmyticket.dto.ShowDTO;
import com.sapient.bookmyticket.dto.ShowSeatDTO;
import com.sapient.bookmyticket.service.BookMyShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bms")
public class BookMyShowController {

    @Autowired
    BookMyShowService bookMyShowService;

    @GetMapping(path = "/search/movieshows", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<ShowDTO> shows(@RequestBody ShowRequest showRequest){
        return bookMyShowService.findAllShowsByMovieAndDateAndCity(showRequest.moviename, showRequest.date, showRequest.city);
    }

    @GetMapping(path = "/search/seats", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<ShowSeatDTO> showSeats(@RequestBody ShowSeatRequest showSeatRequest){
        return bookMyShowService.findAllAvailableSeatsForShow(showSeatRequest.getShowid());
    }

    @PostMapping(path = "/reserve", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String bookSeats(@RequestBody BookingRequest bookingRequest){
        return bookMyShowService.reserveSeats(bookingRequest);
    }

    @PostMapping(path = "/confirm", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String conSeats(@RequestBody BookingRequest bookingRequest){
        return bookMyShowService.confirmSeats(bookingRequest);
    }

}
