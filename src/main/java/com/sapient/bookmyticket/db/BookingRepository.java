package com.sapient.bookmyticket.db;

import com.sapient.bookmyticket.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookingRepository extends JpaRepository<Booking, Long> {
}