package com.sapient.bookmyticket.db;

import com.sapient.bookmyticket.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}