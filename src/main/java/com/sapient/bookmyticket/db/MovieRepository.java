package com.sapient.bookmyticket.db;

import com.sapient.bookmyticket.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie, Long> {
}