package com.sapient.bookmyticket.db;

import com.sapient.bookmyticket.model.Offer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfferRepository extends JpaRepository<Offer, Long> {

}