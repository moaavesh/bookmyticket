package com.sapient.bookmyticket.db;

import com.sapient.bookmyticket.model.Theater;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TheaterRepository extends JpaRepository<Theater, Long> {
}