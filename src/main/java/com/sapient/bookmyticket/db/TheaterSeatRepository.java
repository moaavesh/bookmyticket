package com.sapient.bookmyticket.db;

import com.sapient.bookmyticket.model.Theater;
import com.sapient.bookmyticket.model.TheaterSeat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TheaterSeatRepository extends JpaRepository<TheaterSeat, Long> {

    List<TheaterSeat> findByTheater(Theater theater);
}