package com.sapient.bookmyticket.dto;

import com.sapient.bookmyticket.model.Address;
import com.sapient.bookmyticket.model.Movie;
import com.sapient.bookmyticket.model.Offer;
import com.sapient.bookmyticket.model.Show;
import com.sapient.bookmyticket.offers.OfferType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
public class ShowDTO {
    private Long id;
    private LocalDate date;
    private LocalTime startTime;
    private Movie movie;
    private String theaterName;
    private Long theaterId;
    private Address theaterAddress;
    private List<String> offers;

    public ShowDTO(Show show) {
        this.id = show.getId();
        this.date = show.getDate();
        this.startTime = show.getStartTime();
        this.movie = show.getMovie();
        this.theaterName = show.getTheater().getName();
        this.theaterId = show.getTheater().getId();
        this.theaterAddress = show.getTheater().getAddress();
        this.offers = getOfferDetails(show.getTheater().getOffers());
    }

    private List<String> getOfferDetails(List<Offer> offers) {
        if (offers == null || offers.isEmpty()) {
            return Collections.singletonList(OfferType.OFFER_NOT_APPLICABLE.name());
        }
        List<String> offerList = new ArrayList<>();
        if (offers.stream().anyMatch(offer -> offer.getOfferType().equals(OfferType.AFTERNOON_TICKET))) {
            offerList.add(OfferType.AFTERNOON_TICKET.name() + " with 20% discount");
        }
        if (offers.stream().anyMatch(offer -> offer.getOfferType().equals(OfferType.THIRD_TICKET))) {
            offerList.add(OfferType.THIRD_TICKET.name() + " with 50% discount");
        }
        return offerList;
    }
}
