package com.sapient.bookmyticket.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidBookingException extends Exception{

}
