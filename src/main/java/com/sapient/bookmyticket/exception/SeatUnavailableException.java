package com.sapient.bookmyticket.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class SeatUnavailableException extends Exception{

}
