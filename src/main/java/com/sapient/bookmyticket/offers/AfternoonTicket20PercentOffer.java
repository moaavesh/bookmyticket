package com.sapient.bookmyticket.offers;

import com.sapient.bookmyticket.api.BookingRequest;
import com.sapient.bookmyticket.db.BookingRepository;
import com.sapient.bookmyticket.model.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.List;


@Getter
@Setter
public class AfternoonTicket20PercentOffer implements OfferProcessor {

    public static final double TWENTY_PERCENT_OFF = 0.8;

    BookingRepository bookingRepository;

    private BigDecimal basicPrice;

    private BigDecimal premiumPrice;

    public AfternoonTicket20PercentOffer(BookingRepository bookingRepository, BigDecimal basicPrice, BigDecimal premiumPrice) {
        this.bookingRepository=bookingRepository;
        this.basicPrice=basicPrice;
        this.premiumPrice=premiumPrice;
    }

    @Override
    public void process(List<ShowSeat> showSeats, Booking booking, BookingRequest bookingRequest) {
        if(CollectionUtils.isEmpty(showSeats)) {return;}

        Theater theater = showSeats.get(0).getTheaterSeat().getTheater();
        List<Offer> offers = theater.getOffers();
        if(!CollectionUtils.isEmpty(offers) && offers.stream().anyMatch(offer -> offer.getOfferType().equals(OfferType.AFTERNOON_TICKET))){
            Show show = showSeats.get(0).getShow();
            if(afternoonShow(show)){
                booking.setTotalAmount(booking.getTotalAmount().multiply(BigDecimal.valueOf(TWENTY_PERCENT_OFF)));
                bookingRepository.save(booking);
            }
        }
    }

    private boolean afternoonShow(Show show) {
        // Afternoon is 12pm to 5pm
        LocalTime startTime = show.getStartTime();
        if(startTime.isAfter(LocalTime.parse("11:59")) && startTime.isBefore(LocalTime.parse("17:00"))){
            return true;
        }
        return false;
    }

}
