package com.sapient.bookmyticket.offers;

import com.sapient.bookmyticket.api.BookingRequest;
import com.sapient.bookmyticket.model.Booking;
import com.sapient.bookmyticket.model.ShowSeat;

import java.util.List;

public interface OfferProcessor {

    public void process(List<ShowSeat> showSeats, Booking booking, BookingRequest bookingRequest);

}
