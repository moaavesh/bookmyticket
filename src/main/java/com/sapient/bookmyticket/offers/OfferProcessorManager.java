package com.sapient.bookmyticket.offers;

import com.sapient.bookmyticket.api.BookingRequest;
import com.sapient.bookmyticket.model.Booking;
import com.sapient.bookmyticket.model.ShowSeat;

import java.util.List;

public class OfferProcessorManager {

    List<OfferProcessor> offerProcessors;

    public OfferProcessorManager(List<OfferProcessor> offerProcessors) {
        this.offerProcessors = offerProcessors;
    }

    public void process(List<ShowSeat> showSeats, Booking booking, BookingRequest bookingRequest) {
        offerProcessors.forEach(offerProcessor -> offerProcessor.process(showSeats, booking, bookingRequest));
    }
}
