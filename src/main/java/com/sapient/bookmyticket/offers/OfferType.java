package com.sapient.bookmyticket.offers;

public enum OfferType {
    THIRD_TICKET,
    AFTERNOON_TICKET,
    OFFER_NOT_APPLICABLE
}