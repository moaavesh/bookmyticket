package com.sapient.bookmyticket.offers;

import com.sapient.bookmyticket.api.BookingRequest;
import com.sapient.bookmyticket.db.BookingRepository;
import com.sapient.bookmyticket.model.*;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

public class ThirdTicket50PercentOffer implements OfferProcessor {

    private static final double FIFTY_PERCENT_OFF = 0.5;

    private BookingRepository bookingRepository;

    private BigDecimal basicPrice;

    private BigDecimal premiumPrice;

    public ThirdTicket50PercentOffer(BookingRepository bookingRepository, BigDecimal basicPrice, BigDecimal premiumPrice) {
        this.bookingRepository = bookingRepository;
        this.basicPrice = basicPrice;
        this.premiumPrice = premiumPrice;
    }

    @Override
    public void process(List<ShowSeat> showSeats, Booking booking, BookingRequest bookingRequest) {
        if (CollectionUtils.isEmpty(showSeats)) {
            return;
        }

        Theater theater = showSeats.get(0).getTheaterSeat().getTheater();
        List<Offer> offers = theater.getOffers();
        if (!CollectionUtils.isEmpty(offers) && offers.stream().anyMatch(offer -> offer.getOfferType().equals(OfferType.THIRD_TICKET))) {
            if (bookingRequest.getSeats().size() > 2) {

                BigDecimal amountAfterDiscount = new BigDecimal(0);
                BigDecimal discount = new BigDecimal(0);
                TheaterSeat.SeatType seatType = showSeats.get(2).getTheaterSeat().getSeatType(); // third seat
                switch (seatType) {
                    case BASIC:
                        discount = basicPrice.multiply(BigDecimal.valueOf(FIFTY_PERCENT_OFF));
                        break;
                    case PREMIUM:
                        discount = premiumPrice.multiply(BigDecimal.valueOf(FIFTY_PERCENT_OFF));
                        break;
                }

                amountAfterDiscount = booking.getTotalAmount().subtract(discount);
                booking.setTotalAmount(amountAfterDiscount);
                bookingRepository.save(booking);
            }
        }
    }
}