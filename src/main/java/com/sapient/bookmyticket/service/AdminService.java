package com.sapient.bookmyticket.service;

import com.sapient.bookmyticket.model.*;

import java.util.List;

public interface AdminService {
    void addTheaters(List<Theater> theaters);

    void addMovies(List<Movie> movies);

    void addShows(List<Show> shows);

    void addTheaterSeats(List<TheaterSeat> theaterSeats);

    void addCustomers(List<Customer> customers);

    void addOffers(List<Offer> offers);
}
