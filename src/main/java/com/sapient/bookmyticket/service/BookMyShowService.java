package com.sapient.bookmyticket.service;

import com.sapient.bookmyticket.api.BookingRequest;
import com.sapient.bookmyticket.dto.ShowDTO;
import com.sapient.bookmyticket.dto.ShowSeatDTO;

import java.time.LocalDate;
import java.util.List;

public interface BookMyShowService {

    public List<ShowDTO> findAllShowsByMovieAndDateAndCity(String moviename, LocalDate date, String city);

    public List<ShowSeatDTO> findAllAvailableSeatsForShow(Long showid);

    public String reserveSeats(BookingRequest bookingRequest);

    public String confirmSeats(BookingRequest bookingRequest);

}
